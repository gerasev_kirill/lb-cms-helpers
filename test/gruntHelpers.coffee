# out: false
expect = require('chai').expect
assert = require('chai').assert
log = require('logging').from(__filename)
fs = require('fs')
path = require('path')

gruntHelpers = require('../lib/gruntHelpers')


BASE_DIR = path.dirname(__dirname)
FIXTURES_DIR = BASE_DIR + '/test/fixtures'


isLibNameInList = (list, name, ext)->
    for n in list
        if n.indexOf('/'+name+'/') > -1
            if path.extname(n) == ext
                return true
    false




it 'should return bower js list for index.jade with jquery', (done)->

    # js files with jquery
    bowerIndex = require(FIXTURES_DIR+'/bower/bower.index.json')
    res = gruntHelpers.generateBowerLibsList(FIXTURES_DIR+'/bower/index.jade', 'js', bowerIndex)
    for name, version of bowerIndex.dependencies
        assert(
            isLibNameInList(res, name, '.js'),
            "Expect to find js lib #{name} in res for index.jade"
        )
    # +1 = jQuery
    expect(res.length).to.equal(Object.keys(bowerIndex.dependencies).length + 1)
    done()



it 'should return bower js list for index.jade without jquery', (done)->
    # js files without jquery
    bowerIndexWTJquery = require(FIXTURES_DIR+'/bower/bower.index.w-t-jquery.json')
    res = gruntHelpers.generateBowerLibsList(FIXTURES_DIR+'/bower/index.jade', 'js', bowerIndexWTJquery)
    for name, version of bowerIndexWTJquery.dependencies
        assert(
            isLibNameInList(res, name, '.js'),
            "Expect to find js lib #{name} in res for index.jade"
        )
    expect(res.length).to.equal(Object.keys(bowerIndexWTJquery.dependencies).length)
    done()



it 'should return bower custom js list for index.jade', (done)->
    # js files without jquery
    bowerIndexCustom = require(FIXTURES_DIR+'/bower/bower.index.custom.json')
    res = gruntHelpers.generateBowerLibsList(FIXTURES_DIR+'/bower/index.jade', 'js', bowerIndexCustom)
    for name, version of bowerIndexCustom.dependencies
        assert(
            isLibNameInList(res, name, '.js'),
            "Expect to find js lib #{name} in res for index.jade"
        )
    expect(res.length).to.equal(Object.keys(bowerIndexCustom.dependencies).length)
    done()
