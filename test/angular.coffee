# out: false
expect = require('chai').expect
assert = require('chai').assert
log = require('logging').from(__filename)
fs = require('fs-extra')
path = require('path')

angular = require('../lib/angular')


BASE_DIR = path.dirname(__dirname)



it 'should get google.com with $http', (done)->
    angular.$http.get('http://www.google.com').then \
        (data)->
            console.log 'ok', data
            done()
        ,
        (data)->
            console.log 'fail', data
            done()
