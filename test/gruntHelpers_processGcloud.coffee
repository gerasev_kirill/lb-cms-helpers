# out: false
expect = require('chai').expect
assert = require('chai').assert
log = require('logging').from(__filename)
fs = require('fs-extra')
path = require('path')

gruntHelpers = require('../lib/gruntHelpers')


BASE_DIR = path.dirname(__dirname)
FIXTURES_DIR = BASE_DIR + '/test/fixtures'


fs.emptyDirSync(BASE_DIR+'/test/.fixtures_tmp')
fs.copySync(FIXTURES_DIR, BASE_DIR+'/test/.fixtures_tmp')

FIXTURES_DIR = BASE_DIR+'/test/.fixtures_tmp'


it 'should replace all urls in html and css files', (done)->
    gruntHelpers.processFilesForCloudStorage(
        ['test/.fixtures_tmp/views/index.html', 'test/.fixtures_tmp/client/**/*.html'],
        [],
        'my_custom_bucket',
        "https://storage.googleapis.com/lbcms-container-",
        "/v1.0.1"
    )
    done()
