# out: false
expect = require('chai').expect
assert = require('chai').assert
log = require('logging').from(__filename)
fs = require('fs-extra')
path = require('path')



BASE_DIR = path.dirname(__dirname)


'''
it 'should build from bitbucket', (done)->
    bot = new buildHelpers.BuildBot(
        "https://bitbucket.org/gerasev_kirill/gt2",
        {
            nodeModulesDir: "/home/kirill/work/gt2/node_modules"
            fsGitDir: "/tmp"
            gcloud:{
                projectId: 'phrasal-edition-136623',
                bucket: "lbcms-container-gt2",
                keyFilename: '../lb-cms/server/ktcms-google-key.json',
                asyncLimit: 2
            }
        }
    )
    #bot.build ()->
    bot.deploy(done)
'''

it 'should upload file to aws s3', (done)->
    awsKeys = require('./awsKeys.json')
    buildHelpers = require('../lib/buildHelpers')

    params = JSON.parse(JSON.stringify(awsKeys))
    params.bucket = "lbcms_test_bucket_some_name"
    params.filePrefix = "v9.11.8"

    storage = new buildHelpers.AwsS3Storage(params)
    files = ["test/fixtures/views/index.html", "test/fixtures/client/style.css"]
    storage.uploadFiles files, '', ()->
        done()
