# out: false
expect = require('chai').expect
assert = require('chai').assert
log = require('logging').from(__filename)
fs = require('fs-extra')
path = require('path')

google = require('../lib/google')


BASE_DIR = path.dirname(__dirname)



it 'should get translate word', (done)->
    google.translate('en', 'ru', ['Hello!']).then \
        (data)->
            expect(data['Hello!']).to.equal('Здравствуйте!')
            done()
        ,
        (data)->
            expect(data['Hello!']).to.equal('Здравствуйте!')
            done()
