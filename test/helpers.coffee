# out: false
expect = require('chai').expect
assert = require('chai').assert
log = require('logging').from(__filename)
fs = require('fs-extra')
path = require('path')

H = require('../lib/helpers')


BASE_DIR = path.dirname(__dirname)



it 'should get value by property path', (done)->
    data = {
        'part1' : {
            'name': 'Part 1',
            'size': '20',
            'qty' : '50'
        },
        'part2' : {
            'name': 'Part 2',
            'size': '15',
            'qty' : '60'
        },
        'part3' : [
            {
                'name': 'Part 3A',
                'size': '10',
                'qty' : '20'
            }, {
                'name': 'Part 3B',
                'size': '5',
                'qty' : '20'
            }, {
                'name': 'Part 3C',
                'size': '7.5',
                'qty' : '20'
            }
        ]
    }
    res = H.getValue(data, 'part1.name')
    expect(res).to.equal('Part 1')

    res = H.getValue(data, 'part3[1].name')
    expect(res).to.equal('Part 3B')

    res = H.getValue(data, 'part3[10].name')
    expect(res).to.equal(undefined)

    res = H.getValue(data, 'part3[10].name', 'My Default')
    expect(res).to.equal('My Default')

    res = H.getValueOr(data, 'part1.name', 'part3[10].name', 'My Default')
    expect(res).to.equal('Part 1')

    res = H.getValueOr(data, 'part3[10].name', 'part3[0].name', 'My Default')
    expect(res).to.equal('Part 3A')

    res = H.getValueOr(data, 'part3[10].name', 'part3.no.name', 'My Default')
    expect(res).to.equal('My Default')


    res = H.isEmptyObj(undefined)
    expect(res).to.equal(true)

    res = H.isEmptyObj(null)
    expect(res).to.equal(true)

    res = H.isEmptyObj('')
    expect(res).to.equal(true)
    res = H.isEmptyObj('hello')
    expect(res).to.equal(false)

    res = H.isEmptyObj([])
    expect(res).to.equal(true)
    res = H.isEmptyObj(['hello'])
    expect(res).to.equal(false)

    res = H.isEmptyObj({})
    expect(res).to.equal(true)
    res = H.isEmptyObj({0:1})
    expect(res).to.equal(false)

    done()
