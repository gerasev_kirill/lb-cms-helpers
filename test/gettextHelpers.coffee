# out: false
expect = require('chai').expect
assert = require('chai').assert
log = require('logging').from(__filename)
fs = require('fs-extra')
path = require('path')

gettextHelpers = require('../lib/gettextHelpers')


BASE_DIR = path.dirname(__dirname)
FIXTURES_DIR = BASE_DIR + '/test/fixtures'



it 'should convert *.pot files to json', (done)->
    res = gettextHelpers.potToJson(FIXTURES_DIR+'/po/**/*.pot')
    expectedRes = require(FIXTURES_DIR+'/po/res.json')
    expect(res).to.deep.equal(expectedRes)
    done()


it 'should merge two msgs-json into one json', (done)->
    _original = require(FIXTURES_DIR+'/translations/originalMsgs.json')
    _new = require(FIXTURES_DIR+'/translations/newMsgs.json')
    _merged = require(FIXTURES_DIR+'/translations/mergedMsgs.json')

    res = gettextHelpers.mergeTranslationMsgs(_original.msgs, _new.msgs)
    expect(res).to.deep.equal(_merged)
    done()
