
pofile = require('pofile')
glob = require('glob')
path = require('path')
fs = require('fs')
htmlSafeCodes = require('./htmlSafeCodes')


makeUnSafeString = (string)->
    for code, value of htmlSafeCodes
        codePattern = new RegExp( '&' + code + ';?', 'g' )
        string = string.replace(codePattern, value)
    string



potToJson = (filePath, options={})->
    ###
        преобразовывает *.pot файлы в CmsTranslations-подобные объекты
        @param {string | Array<string>} путь к *.pot файлу или список путей
            (можно передавать glob-паттерны)
        @returns {object} CmsTranslations-подобный объект
    ###
    if typeof(filePath) == 'string'
        filePath = [filePath]
    p = []
    for f in filePath
        list = glob.sync(f)
        p = p.concat(list)
    pot_data = []
    for f in p
        ext = path.extname(f)
        if ext == '.pot'
            data = fs.readFileSync(f, 'utf8')
            data = pofile.parse(data)
            pot_data.push(data)

    msgs = []
    for catalog in pot_data
        for item in catalog.items
            obj = {msgId: makeUnSafeString(item.msgid)}
            if item.msgid_plural
                obj.msgIdPlural = makeUnSafeString(item.msgid_plural)
            if item.msgctxt
                obj.msgCtxt = item.msgctxt
            if item.extractedComments.length
                comm = item.extractedComments.join(' ')
                if comm.length > 1
                    obj.msgComm = comm
            if options.withReferences
                obj.references = item.references or []
            msgs.push(obj)

    return {msgs}


mergeTranslationMsgs = (originalMsgs, newMsgs)->
    getId = (item)->
        id = item.msgId
        if item.msgCtxt
            id += item.msgCtxt
        id

    getIds = (msgs)->
        (getId(item) for item in msgs)

    updateProperty = (_original, _new, field)->
        if _original[field] and _new[field]
            _original[field] = _new[field]
        else if _original[field] and !_new[field]
            delete _original[field]
        else if !_original[field] and _new[field]
            _original[field] = _new[field]
        _original

    allowedIds = getIds(newMsgs)
    existedIds = []
    processedNewMsgs = {}

    for item in newMsgs
        id = getId(item)
        processedNewMsgs[id] = item

    for item,i in originalMsgs
        id = getId(item)
        if allowedIds.indexOf(id) == -1
            originalMsgs[i].hidden = true
        else
            existedIds.push(id)
            originalMsgs[i].hidden = false
            for prop in ['msgIdPlural', 'msgCtxt', 'msgComm']
                originalMsgs[i] = updateProperty(
                    originalMsgs[i],
                    processedNewMsgs[id],
                    prop
                )
    for id in allowedIds when existedIds.indexOf(id) == -1
        originalMsgs.push(processedNewMsgs[id])
        originalMsgs[originalMsgs.length-1].hidden = false

    originalMsgs






module.exports = {
    potToJson
    mergeTranslationMsgs
}
