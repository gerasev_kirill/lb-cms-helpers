(function() {
  var $http, $q, GOOGLE_API_KEY, GoogleTranslate, SafeGoogleTranslateString, fixLangCode, gtranslate, helpers;

  GOOGLE_API_KEY = "AIzaSyDqPnzSiMCuFXwOe28LeNC4ZUSHzWxM31k";

  $http = require('./angular').$http;

  $q = require('./angular').$q;

  helpers = require('./helpers');

  SafeGoogleTranslateString = (function() {
    function SafeGoogleTranslateString() {
      this.regExp = /\{\{([A-Za-z0-9_\s+\(+\)+\$+\.+]+)\}\}/g;
      this.regExp = /\{\{([^\}]+)\}\}/g;
      this.strings = {};
    }

    SafeGoogleTranslateString.prototype.makeSafe = function(text) {
      var j, len, match, matches, ng_exprs, safe_expr, safe_text;
      matches = text.matchAll(this.regExp);
      if (!matches) {
        return encodeURIComponent(text);
      }
      ng_exprs = {};
      safe_text = angular.copy(text);
      for (j = 0, len = matches.length; j < len; j++) {
        match = matches[j];
        safe_expr = match[0].replaceAll("{{", "<span class='notranslate'>{{").replaceAll("}}", '}}</span>');
        if (!ng_exprs[safe_expr]) {
          ng_exprs[safe_expr] = match[0];
          safe_text = safe_text.replaceAll(match[0], safe_expr);
        }
      }
      this.strings[text] = ng_exprs;
      return encodeURIComponent(safe_text);
    };

    SafeGoogleTranslateString.prototype.makeUnsafe = function(keyText, text) {
      var expr, ref, value;
      if (!this.strings[keyText]) {
        return text;
      }
      ref = this.strings[keyText];
      for (expr in ref) {
        value = ref[expr];
        text = text.replaceAll(expr, value);
      }
      return text;
    };

    return SafeGoogleTranslateString;

  })();

  fixLangCode = function(lang) {
    if (lang === 'cz') {
      return 'cs';
    }
    if (lang === 'ua') {
      return 'uk';
    }
    return lang;
  };

  GoogleTranslate = (function() {
    function GoogleTranslate() {
      this.cache = {};
    }

    GoogleTranslate.prototype.findTranslationsInCache = function(fromTo, text_list) {
      var j, len, no_tr, text, tr;
      if (!this.cache[fromTo]) {
        this.cache[fromTo] = {};
        return;
      }
      tr = {};
      no_tr = [];
      for (j = 0, len = text_list.length; j < len; j++) {
        text = text_list[j];
        if (this.cache[fromTo][text]) {
          tr[text] = this.cache[fromTo][text];
        } else {
          no_tr.push(text);
        }
      }
      return {
        tr: tr,
        noTr: no_tr
      };
    };

    GoogleTranslate.prototype.saveTranslationsToCache = function(fromTo, fromTextList, toGtranslate, sgt) {
      var i, j, len, text, tr, unsafe_translation;
      if (!toGtranslate || !toGtranslate.translations) {
        return;
      }
      tr = {};
      for (i = j = 0, len = fromTextList.length; j < len; i = ++j) {
        text = fromTextList[i];
        unsafe_translation = sgt.makeUnsafe(text, toGtranslate.translations[i].translatedText);
        tr[text] = unsafe_translation;
        this.cache[fromTo][text] = unsafe_translation;
      }
      return tr;
    };

    GoogleTranslate.prototype.translate = function(sourceLang, targetLang, textList) {
      var _textList, deferred, j, len, res, self, sgt, t, url;
      sourceLang = fixLangCode(sourceLang);
      targetLang = fixLangCode(targetLang);
      deferred = $q.defer('hello');
      self = this;
      res = this.findTranslationsInCache(sourceLang + "-" + targetLang, textList);
      if (!res || helpers.isEmptyObj(res.tr)) {
        _textList = textList;
      } else {
        _textList = res.noTr;
      }
      _textList = textList;
      res = null;
      if (!_textList.length && res) {
        deferred.resolve(res.tr);
        return deferred.promise;
      }
      sgt = new SafeGoogleTranslateString();
      url = ("https://www.googleapis.com/language/translate/v2?key=" + GOOGLE_API_KEY) + ("&source=" + sourceLang + "&target=" + targetLang + "&format=html");
      for (j = 0, len = _textList.length; j < len; j++) {
        t = _textList[j];
        url += "&q=" + (sgt.makeSafe(t));
      }
      $http.get(url).then(function(data) {
        var tr;
        tr = self.saveTranslationsToCache(sourceLang + "-" + targetLang, _textList, data.data, sgt);
        if (res) {
          tr = angular.extend(res.tr, tr);
        }
        return deferred.resolve(tr);
      }, function(data) {
        return deferred.reject();
      });
      return deferred.promise;
    };

    return GoogleTranslate;

  })();

  gtranslate = new GoogleTranslate();

  module.exports = {
    translate: function(from, to, list) {
      return gtranslate.translate(from, to, list);
    }
  };

}).call(this);
