
GOOGLE_API_KEY = "AIzaSyDqPnzSiMCuFXwOe28LeNC4ZUSHzWxM31k"

$http = require('./angular').$http
$q = require('./angular').$q
helpers = require('./helpers')


class SafeGoogleTranslateString
    constructor: ()->
        @regExp = /\{\{([A-Za-z0-9_\s+\(+\)+\$+\.+]+)\}\}/g
        @regExp = /\{\{([^\}]+)\}\}/g
        @strings = {}

    makeSafe: (text)->
        matches = text.matchAll(@regExp)
        if !matches
            # забавно, но тут нужно их тоже ескейпить(в отличии от клиентской реализации)
            return encodeURIComponent(text)
        ng_exprs = {}
        safe_text = angular.copy(text)
        for match in matches
            safe_expr = match[0].replaceAll("{{", "<span class='notranslate'>{{").replaceAll("}}", '}}</span>')
            if !ng_exprs[safe_expr]
                ng_exprs[safe_expr] = match[0]
                safe_text = safe_text.replaceAll(match[0], safe_expr)
        @strings[text] = ng_exprs
        encodeURIComponent(safe_text)

    makeUnsafe: (keyText, text)->
        if !@strings[keyText]
            return text
        for expr,value of @strings[keyText]
            text = text.replaceAll(expr, value)
        text




fixLangCode = (lang)->
    # гугль транслейт использует чуть другие коды для языков
    if lang == 'cz'
        return 'cs'
    if lang == 'ua'
        return 'uk'
    lang


class GoogleTranslate
    constructor: ()->
        @cache = {}

    findTranslationsInCache: (fromTo, text_list)->
        if !@cache[fromTo]
            @cache[fromTo] = {}
            return
        tr = {}
        no_tr = []
        for text in text_list
            if @cache[fromTo][text]
                tr[text] = @cache[fromTo][text]
            else
                no_tr.push(text)
        {
            tr: tr
            noTr: no_tr
        }

    saveTranslationsToCache: (fromTo, fromTextList, toGtranslate, sgt)->
        if !toGtranslate or !toGtranslate.translations
            return

        tr = {}
        for text,i in fromTextList
            unsafe_translation = sgt.makeUnsafe(
                text,
                toGtranslate.translations[i].translatedText
            )
            tr[text] = unsafe_translation
            @cache[fromTo][text] = unsafe_translation
        tr



    translate: (sourceLang, targetLang, textList)->
        sourceLang = fixLangCode(sourceLang)
        targetLang = fixLangCode(targetLang)
        deferred = $q.defer('hello')
        self = @
        res = @findTranslationsInCache(sourceLang+"-"+targetLang, textList)
        if !res or helpers.isEmptyObj(res.tr)
            _textList = textList
        else
            _textList = res.noTr
        _textList = textList
        res = null
        if !_textList.length and res
            deferred.resolve(res.tr)
            return deferred.promise

        sgt = new SafeGoogleTranslateString()

        url = "https://www.googleapis.com/language/translate/v2?key=#{GOOGLE_API_KEY}" \
             + "&source=#{sourceLang}&target=#{targetLang}&format=html"
        for t in _textList
            url += "&q=#{sgt.makeSafe(t)}"

        $http.get(url).then \
            (data)->
                tr = self.saveTranslationsToCache(sourceLang+"-"+targetLang, _textList, data.data, sgt)
                if res
                    tr = angular.extend(res.tr, tr)
                deferred.resolve(tr)
            ,
            (data)->
                deferred.reject()

        deferred.promise


gtranslate = new GoogleTranslate()

module.exports = {
    translate: (from, to, list)->
        gtranslate.translate(from, to, list)
}
