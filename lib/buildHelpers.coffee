
exec = require('child_process').exec
path = require('path')
fs = require('fs-extra')
glob = require('glob')
AWS = require('aws-sdk')
async = require('async')
try
    log = require('logging').from(__filename)
catch error
    log = console.log

gettextHelpers = require('./gettextHelpers')
gruntHelpers = require('./gruntHelpers')



class GCloudStorage
    constructor: (@options)->
        try
            cloudStorage = require('@google-cloud/storage')
        catch error
            throw "Please install gcloud package!"

        @errors = []
        @storage = cloudStorage({
            projectId: @options.projectId,
            keyFilename: @options.keyFilename
        })
        @bucket = @storage.bucket(@options.bucket)
        @defaultAcl = {
            entity: 'allUsers'
            role: @storage.acl.READER_ROLE
        }


    checkBucketStatus: (ondone)->
        @bucket.exists (err, exists)=>
            if err
                @errors.push(err)
                return ondone(err)
            if exists
                return ondone()
            options = {
                multiRegional: true
                location: 'EU'
            }
            @storage.createBucket @options.bucket, options, (err, bucket)=>
                if err
                    @errors.push(err)
                    return ondone(err)
                @bucket = bucket
                @bucket.acl.default.add @defaultAcl, (err)->
                    if err
                        @errors.push(err)
                    ondone(err)


    uploadFiles: (files, baseDir, ondone)->
        asyncTasks = []
        errors = []
        ondone or= ()->

        for file in files
            asyncTasks.push do(file)=>
                (asyncCallback)=>
                    if file[0] == '"'
                        file = file.substr(1)
                    options = {
                        gzip:true
                        destination: file.replace(baseDir,'')
                        predefinedAcl: 'publicRead'
                    }
                    if options.destination[0] == '.'
                        options.destination = options.destination.substr(1)
                    if options.destination[0] == '/'
                        options.destination = options.destination.substr(1)
                    if !fs.existsSync(file) and file[0] == '/'
                        file = file.substr(1)
                    if !fs.existsSync(file)
                        log "!File: #{file} - doesn't exists!"
                        return asyncCallback()
                    else
                        log '[Upload: '+ file + ' ]'
                    @bucket.upload file, options, (err, f)->
                        if err
                            errors.push(err)
                        asyncCallback()

        @checkBucketStatus (err)=>
            if err
                log "*Bucket check: #{err}*"
                return ondone()
            if async.parallelLimit
                async.parallelLimit(
                    asyncTasks,
                    @options.asyncLimit || 5,
                    ondone
                )
            else
                async.parallel(asyncTasks, ondone)







class AwsS3Storage
    constructor: (@options)->
        AWS.config.update({
            accessKeyId: @options.accessKeyId,
            secretAccessKey: @options.secretAccessKey
        })
        @errors = []
        @bucket = @options.bucket
        @defaultAcl = 'public-read'
        @s3 = new AWS.S3({
            endpoint:  new AWS.Endpoint(@options.storageEndpoint)
        })
        @isDigitalOcean = @options.storageEndpoint.indexOf('.digitaloceanspaces.com') > -1


    checkBucketStatus: (ondone)->
        if @isDigitalOcean
            ondone()
            return
        @s3.headBucket {Bucket: @bucket}, (err, data)=>
            if err and err.statusCode == 404
                # бакет не существует
                params = {
                    Bucket: @bucket
                    CreateBucketConfiguration: {
                        LocationConstraint: "eu-west-1"
                    }
                }
                @s3.createBucket params, (err, data)=>
                    if err
                        @errors.push(err)
                        return ondone(err)
                    # ожидаем пока бакет действительно создастся
                    @s3.waitFor 'bucketExists', {Bucket: @bucket}, (err, data)=>
                        if err
                            @errors.push(err)
                            return ondone(err)
                        # бакет создан и готов к использованию
                        ondone()
            else
                if err
                    @errors.push(err)
                return ondone(err)
        return


    uploadFiles: (files, baseDir, ondone)->
        asyncTasks = []
        errors = []
        ondone or= ()->
        mime = require('mime-types')
        zlib = require('zlib')

        for file,i in files
            asyncTasks.push do(file)=>
                (asyncCallback)=>
                    if file[0] == '"'
                        file = file.substr(1)
                    options = {
                        Key: file.replace(baseDir,'')
                        Bucket: @bucket
                        ACL: @defaultAcl
                        InputSerialization: {
                            CompressionType: "GZIP"
                        }
                    }
                    if options.Key[0] == '.'
                        options.Key = options.Key.substr(1)
                    if options.Key[0] == '/'
                        options.Key = options.Key.substr(1)
                    if !fs.existsSync(file) and file[0] == '/'
                        file = file.substr(1)
                    if !fs.existsSync(file)
                        log "!File: #{file} - doesn't exists!"
                        return asyncCallback()

                    options.Body = fs.readFileSync(file)
                    options.ContentType = mime.lookup(file) or 'application/octet-stream'
                    if options.ContentType.indexOf('video/') != 0
                        # добавляем gzip-сжатие
                        options.Body = zlib.gzipSync(options.Body)
                        options.ContentEncoding = 'gzip'

                    if @options.filePrefix
                        options.Key = path.join(@options.filePrefix, file)
                        if options.Key[0] == '/'
                            options.Key = options.Key.substring(1)
                        log "[Upload: '#{file}' to '#{options.Key}' ]"
                    else
                        log "[Upload: #{options.Key} ]"

                    @s3.upload options, (err, f)->
                        if err
                            errors.push(err)
                        asyncCallback()

        @checkBucketStatus (err)=>
            if err
                log "*Bucket check: #{err}*"
                return ondone()
            if async.parallelLimit
                async.parallelLimit(
                    asyncTasks,
                    @options.asyncLimit || 5,
                    ondone
                )
            else
                async.parallel(asyncTasks, ondone)







class BuildBot
    constructor: (@repoUrl, options)->
        @nodeModulesDir = options.nodeModulesDir
        @fsGitDir = options.fsGitDir
        @pathToSshKey = options.pathToSshKey
        @bucket = options.gcloud.bucket
        @options = options
        if @pathToSshKey and !options.isPrivateRepo
            @GIT_SSH_COMMAND = "GIT_SSH_COMMAND='ssh -i #{@pathToSshKey}'"
        else
            @GIT_SSH_COMMAND = ''
        @name = path.parse(@repoUrl).name
        @gitPath = path.join(@fsGitDir, @name)
        @errors = []
        @stdouts = []

    exec: (command, alias, ondone)->
        exec command, (error, stdout, stderr)=>
            console.log alias
            e = {alias}
            s = {alias}
            if error
                e.error = error
            if stderr
                e.stderr = stderr
            if error or stderr
                @errors.push(e)
            if stdout
                s.stdout = stdout
                @stdouts.push(s)
            console.log @error
            ondone(error)


    fetchAndUpdate: (ondone)->
        if fs.existsSync(@gitPath+'/.git')
            command = [
                "cd #{@gitPath}"
                "git reset --hard"
                @GIT_SSH_COMMAND + " git fetch --all"
            ]
            command = command.join('; ')
        else
            command = @GIT_SSH_COMMAND + " git clone #{@repoUrl} #{@gitPath}"
        if @options.hash
            command += "; git checkout #{@options.hash}"
        @exec command, 'fetchAndUpdate', (error)->
            if ondone then ondone(error)


    cleanLocalRepo: (ondone)->
        has_clean_target = false
        if fs.existsSync(@gitPath+'/Makefile')
            data = fs.readFileSync(@gitPath+'/Makefile', 'utf8')
            for line in data.split('\n')
                if line.indexOf('clean:')==0 or line.indexOf('clean :')==0
                    has_clean_target = true
                    continue
        if !has_clean_target
            if ondone
                ondone()
            return
        command = "cd #{@gitPath}; make clean"
        @exec command, 'cleanLocalRepo', (error)->
            if ondone then ondone(error)

    buildSite: (ondone)->
        node_modules_dir = @gitPath+'/node_modules'
        fs.removeSync(node_modules_dir)
        fs.ensureSymlinkSync(@nodeModulesDir, node_modules_dir)
        command = "cd #{@gitPath}; bower install; grunt build-prod;"
        exec command, 'buildSite', (error)->
            if ondone then ondone(error)

    collectTranslations: (ondone)->
        @gettextJson = gettextHelpers.potToJson(@gitPath+"/po/**/*.pot")
        if ondone then ondone()


    build: (ondone)->
        @fetchAndUpdate (e)=>
            if e
                if ondone then ondone(e)
                return
            @cleanLocalRepo (e)=>
                if e
                    if ondone then ondone(e)
                    return
                @buildSite (e)=>
                    if e
                        if ondone then ondone(e)
                        return
                    @collectTranslations (e)=>
                        if ondone then ondone(e)


    deploy: (ondone)->
        patterns = [
            @gitPath+'/client/**/*.min.js'
            @gitPath+'/client/**/*.min.css'
            @gitPath+'/client/**/*.{jpg,png,gif,svg}'
        ]
        files = []
        for p in patterns
            list = glob.sync(p)
            files = files.concat(list)

        storage = new GCloudStorage(@options.gcloud)
        storage.uploadFiles files, @gitPath, ()=>
            if storage.errors.length
                @errors.push({alias: 'deploy to google cloud', error: storage.errors})
            gce_zone = @options.gcloud.gceZone or 'europe-west1-d'
            vm = @options.gcloud.vm or 'shared-hosting--git'
            bucket = @options.gcloud.bucket.replace('lbcms-container-', '')
            files = {
                'views/*.html': 'public_html/views/'
                'views/index.html': 'public_html/index.html'
                '_htaccess': 'public_html/.htaccess'
            }
            command = ""
            for k,v of files
                command += "gcloud compute --project #{@options.gcloud.projectId} copy-files #{@gitPath}/#{k} node-user@#{vm}:/var/www/#{bucket}/#{v} --zone #{gce_zone}; "
            @exec command, "upload files to compute engine", ()->
                if ondone
                    ondone()





module.exports = {
    BuildBot
    GCloudStorage
    AwsS3Storage
}
