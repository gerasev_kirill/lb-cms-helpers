(function() {
  var $http, Q, request;

  request = require('request');

  Q = (function() {
    function Q(name) {
      var self;
      this.name = name;
      self = this;
      this.done = false;
      this.promise = {
        then: function(onSuccess, onErr) {
          if (self.rejected && !self.done) {
            self.done = true;
            return onErr(self.data);
          }
          if (self.resolved && !self.done) {
            self.done = true;
            return onSuccess(self.data);
          }
          self.onErr = onErr;
          self.onSuccess = onSuccess;
        }
      };
    }

    Q.prototype.resolve = function(data) {
      this.resolved = true;
      this.data = data;
      if (this.onSuccess && !this.done) {
        this.done = true;
        this.onSuccess(data);
      }
    };

    Q.prototype.reject = function(data) {
      this.rejected = true;
      this.data = data;
      if (this.onErr && !this.done) {
        this.done = true;
        this.onErr(data);
      }
    };

    return Q;

  })();

  $http = {
    get: function(url) {
      var defer, options;
      defer = new Q('http');
      options = {
        url: url,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Referer": "http://localhost:3000"
        }
      };
      request.get(options, function(err, res, body) {
        var data, e;
        if (err) {
          return defer.reject(err);
        }
        data = body;
        try {
          data = JSON.parse(body);
        } catch (error) {
          e = error;
        }
        return defer.resolve(data);
      });
      return defer.promise;
    }
  };

  String.prototype.matchAll = function(regexp) {
    var matches;
    matches = [];
    this.replace(regexp, function() {
      var arr, extras;
      arr = [].slice.call(arguments, 0);
      extras = arr.splice(-2);
      arr.index = extras[0];
      arr.input = extras[1];
      matches.push(arr);
    });
    if (matches.length) {
      return matches;
    } else {
      return null;
    }
  };

  String.prototype.replaceAll = function(search, replacement) {
    var target;
    target = this;
    if (search instanceof RegExp) {
      target = target.replace(new RegExp(search, 'g'), replacement);
    } else {
      search = angular.copy(search) + '';
      target = target.split(search).join(replacement);
    }
    return target;
  };

  module.exports = {
    $http: $http,
    $q: {
      defer: function() {
        return new Q();
      }
    }
  };

}).call(this);
