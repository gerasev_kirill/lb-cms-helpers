(function() {
  var fs, glob, htmlSafeCodes, makeUnSafeString, mergeTranslationMsgs, path, pofile, potToJson;

  pofile = require('pofile');

  glob = require('glob');

  path = require('path');

  fs = require('fs');

  htmlSafeCodes = require('./htmlSafeCodes');

  makeUnSafeString = function(string) {
    var code, codePattern, value;
    for (code in htmlSafeCodes) {
      value = htmlSafeCodes[code];
      codePattern = new RegExp('&' + code + ';?', 'g');
      string = string.replace(codePattern, value);
    }
    return string;
  };

  potToJson = function(filePath, options) {
    var catalog, comm, data, ext, f, item, j, k, l, len, len1, len2, len3, list, m, msgs, obj, p, pot_data, ref;
    if (options == null) {
      options = {};
    }

    /*
        преобразовывает *.pot файлы в CmsTranslations-подобные объекты
        @param {string | Array<string>} путь к *.pot файлу или список путей
            (можно передавать glob-паттерны)
        @returns {object} CmsTranslations-подобный объект
     */
    if (typeof filePath === 'string') {
      filePath = [filePath];
    }
    p = [];
    for (j = 0, len = filePath.length; j < len; j++) {
      f = filePath[j];
      list = glob.sync(f);
      p = p.concat(list);
    }
    pot_data = [];
    for (k = 0, len1 = p.length; k < len1; k++) {
      f = p[k];
      ext = path.extname(f);
      if (ext === '.pot') {
        data = fs.readFileSync(f, 'utf8');
        data = pofile.parse(data);
        pot_data.push(data);
      }
    }
    msgs = [];
    for (l = 0, len2 = pot_data.length; l < len2; l++) {
      catalog = pot_data[l];
      ref = catalog.items;
      for (m = 0, len3 = ref.length; m < len3; m++) {
        item = ref[m];
        obj = {
          msgId: makeUnSafeString(item.msgid)
        };
        if (item.msgid_plural) {
          obj.msgIdPlural = makeUnSafeString(item.msgid_plural);
        }
        if (item.msgctxt) {
          obj.msgCtxt = item.msgctxt;
        }
        if (item.extractedComments.length) {
          comm = item.extractedComments.join(' ');
          if (comm.length > 1) {
            obj.msgComm = comm;
          }
        }
        if (options.withReferences) {
          obj.references = item.references || [];
        }
        msgs.push(obj);
      }
    }
    return {
      msgs: msgs
    };
  };

  mergeTranslationMsgs = function(originalMsgs, newMsgs) {
    var allowedIds, existedIds, getId, getIds, i, id, item, j, k, l, len, len1, len2, len3, m, processedNewMsgs, prop, ref, updateProperty;
    getId = function(item) {
      var id;
      id = item.msgId;
      if (item.msgCtxt) {
        id += item.msgCtxt;
      }
      return id;
    };
    getIds = function(msgs) {
      var item, j, len, results;
      results = [];
      for (j = 0, len = msgs.length; j < len; j++) {
        item = msgs[j];
        results.push(getId(item));
      }
      return results;
    };
    updateProperty = function(_original, _new, field) {
      if (_original[field] && _new[field]) {
        _original[field] = _new[field];
      } else if (_original[field] && !_new[field]) {
        delete _original[field];
      } else if (!_original[field] && _new[field]) {
        _original[field] = _new[field];
      }
      return _original;
    };
    allowedIds = getIds(newMsgs);
    existedIds = [];
    processedNewMsgs = {};
    for (j = 0, len = newMsgs.length; j < len; j++) {
      item = newMsgs[j];
      id = getId(item);
      processedNewMsgs[id] = item;
    }
    for (i = k = 0, len1 = originalMsgs.length; k < len1; i = ++k) {
      item = originalMsgs[i];
      id = getId(item);
      if (allowedIds.indexOf(id) === -1) {
        originalMsgs[i].hidden = true;
      } else {
        existedIds.push(id);
        originalMsgs[i].hidden = false;
        ref = ['msgIdPlural', 'msgCtxt', 'msgComm'];
        for (l = 0, len2 = ref.length; l < len2; l++) {
          prop = ref[l];
          originalMsgs[i] = updateProperty(originalMsgs[i], processedNewMsgs[id], prop);
        }
      }
    }
    for (m = 0, len3 = allowedIds.length; m < len3; m++) {
      id = allowedIds[m];
      if (!(existedIds.indexOf(id) === -1)) {
        continue;
      }
      originalMsgs.push(processedNewMsgs[id]);
      originalMsgs[originalMsgs.length - 1].hidden = false;
    }
    return originalMsgs;
  };

  module.exports = {
    potToJson: potToJson,
    mergeTranslationMsgs: mergeTranslationMsgs
  };

}).call(this);
