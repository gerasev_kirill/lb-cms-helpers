var AWS, AwsS3Storage, BuildBot, GCloudStorage, async, error, exec, fs, gettextHelpers, glob, gruntHelpers, log, path;

exec = require('child_process').exec;

path = require('path');

fs = require('fs-extra');

glob = require('glob');

AWS = require('aws-sdk');

async = require('async');

try {
  log = require('logging').from(__filename);
} catch (error1) {
  error = error1;
  log = console.log;
}

gettextHelpers = require('./gettextHelpers');

gruntHelpers = require('./gruntHelpers');

GCloudStorage = (function() {
  function GCloudStorage(options1) {
    var cloudStorage;
    this.options = options1;
    try {
      cloudStorage = require('@google-cloud/storage');
    } catch (error1) {
      error = error1;
      throw "Please install gcloud package!";
    }
    this.errors = [];
    this.storage = cloudStorage({
      projectId: this.options.projectId,
      keyFilename: this.options.keyFilename
    });
    this.bucket = this.storage.bucket(this.options.bucket);
    this.defaultAcl = {
      entity: 'allUsers',
      role: this.storage.acl.READER_ROLE
    };
  }

  GCloudStorage.prototype.checkBucketStatus = function(ondone) {
    return this.bucket.exists((function(_this) {
      return function(err, exists) {
        var options;
        if (err) {
          _this.errors.push(err);
          return ondone(err);
        }
        if (exists) {
          return ondone();
        }
        options = {
          multiRegional: true,
          location: 'EU'
        };
        return _this.storage.createBucket(_this.options.bucket, options, function(err, bucket) {
          if (err) {
            _this.errors.push(err);
            return ondone(err);
          }
          _this.bucket = bucket;
          return _this.bucket.acl["default"].add(_this.defaultAcl, function(err) {
            if (err) {
              this.errors.push(err);
            }
            return ondone(err);
          });
        });
      };
    })(this));
  };

  GCloudStorage.prototype.uploadFiles = function(files, baseDir, ondone) {
    var asyncTasks, errors, file, j, len;
    asyncTasks = [];
    errors = [];
    ondone || (ondone = function() {});
    for (j = 0, len = files.length; j < len; j++) {
      file = files[j];
      asyncTasks.push((function(_this) {
        return function(file) {
          return function(asyncCallback) {
            var options;
            if (file[0] === '"') {
              file = file.substr(1);
            }
            options = {
              gzip: true,
              destination: file.replace(baseDir, ''),
              predefinedAcl: 'publicRead'
            };
            if (options.destination[0] === '.') {
              options.destination = options.destination.substr(1);
            }
            if (options.destination[0] === '/') {
              options.destination = options.destination.substr(1);
            }
            if (!fs.existsSync(file) && file[0] === '/') {
              file = file.substr(1);
            }
            if (!fs.existsSync(file)) {
              log("!File: " + file + " - doesn't exists!");
              return asyncCallback();
            } else {
              log('[Upload: ' + file + ' ]');
            }
            return _this.bucket.upload(file, options, function(err, f) {
              if (err) {
                errors.push(err);
              }
              return asyncCallback();
            });
          };
        };
      })(this)(file));
    }
    return this.checkBucketStatus((function(_this) {
      return function(err) {
        if (err) {
          log("*Bucket check: " + err + "*");
          return ondone();
        }
        if (async.parallelLimit) {
          return async.parallelLimit(asyncTasks, _this.options.asyncLimit || 5, ondone);
        } else {
          return async.parallel(asyncTasks, ondone);
        }
      };
    })(this));
  };

  return GCloudStorage;

})();

AwsS3Storage = (function() {
  function AwsS3Storage(options1) {
    this.options = options1;
    AWS.config.update({
      accessKeyId: this.options.accessKeyId,
      secretAccessKey: this.options.secretAccessKey
    });
    this.errors = [];
    this.bucket = this.options.bucket;
    this.defaultAcl = 'public-read';
    this.s3 = new AWS.S3({
      endpoint: new AWS.Endpoint(this.options.storageEndpoint)
    });
    this.isDigitalOcean = this.options.storageEndpoint.indexOf('.digitaloceanspaces.com') > -1;
  }

  AwsS3Storage.prototype.checkBucketStatus = function(ondone) {
    if (this.isDigitalOcean) {
      ondone();
      return;
    }
    this.s3.headBucket({
      Bucket: this.bucket
    }, (function(_this) {
      return function(err, data) {
        var params;
        if (err && err.statusCode === 404) {
          params = {
            Bucket: _this.bucket,
            CreateBucketConfiguration: {
              LocationConstraint: "eu-west-1"
            }
          };
          return _this.s3.createBucket(params, function(err, data) {
            if (err) {
              _this.errors.push(err);
              return ondone(err);
            }
            return _this.s3.waitFor('bucketExists', {
              Bucket: _this.bucket
            }, function(err, data) {
              if (err) {
                _this.errors.push(err);
                return ondone(err);
              }
              return ondone();
            });
          });
        } else {
          if (err) {
            _this.errors.push(err);
          }
          return ondone(err);
        }
      };
    })(this));
  };

  AwsS3Storage.prototype.uploadFiles = function(files, baseDir, ondone) {
    var asyncTasks, errors, file, i, j, len, mime, zlib;
    asyncTasks = [];
    errors = [];
    ondone || (ondone = function() {});
    mime = require('mime-types');
    zlib = require('zlib');
    for (i = j = 0, len = files.length; j < len; i = ++j) {
      file = files[i];
      asyncTasks.push((function(_this) {
        return function(file) {
          return function(asyncCallback) {
            var options;
            if (file[0] === '"') {
              file = file.substr(1);
            }
            options = {
              Key: file.replace(baseDir, ''),
              Bucket: _this.bucket,
              ACL: _this.defaultAcl,
              InputSerialization: {
                CompressionType: "GZIP"
              }
            };
            if (options.Key[0] === '.') {
              options.Key = options.Key.substr(1);
            }
            if (options.Key[0] === '/') {
              options.Key = options.Key.substr(1);
            }
            if (!fs.existsSync(file) && file[0] === '/') {
              file = file.substr(1);
            }
            if (!fs.existsSync(file)) {
              log("!File: " + file + " - doesn't exists!");
              return asyncCallback();
            }
            options.Body = fs.readFileSync(file);
            options.ContentType = mime.lookup(file) || 'application/octet-stream';
            if (options.ContentType.indexOf('video/') !== 0) {
              options.Body = zlib.gzipSync(options.Body);
              options.ContentEncoding = 'gzip';
            }
            if (_this.options.filePrefix) {
              options.Key = path.join(_this.options.filePrefix, file);
              if (options.Key[0] === '/') {
                options.Key = options.Key.substring(1);
              }
              log("[Upload: '" + file + "' to '" + options.Key + "' ]");
            } else {
              log("[Upload: " + options.Key + " ]");
            }
            return _this.s3.upload(options, function(err, f) {
              if (err) {
                errors.push(err);
              }
              return asyncCallback();
            });
          };
        };
      })(this)(file));
    }
    return this.checkBucketStatus((function(_this) {
      return function(err) {
        if (err) {
          log("*Bucket check: " + err + "*");
          return ondone();
        }
        if (async.parallelLimit) {
          return async.parallelLimit(asyncTasks, _this.options.asyncLimit || 5, ondone);
        } else {
          return async.parallel(asyncTasks, ondone);
        }
      };
    })(this));
  };

  return AwsS3Storage;

})();

BuildBot = (function() {
  function BuildBot(repoUrl, options) {
    this.repoUrl = repoUrl;
    this.nodeModulesDir = options.nodeModulesDir;
    this.fsGitDir = options.fsGitDir;
    this.pathToSshKey = options.pathToSshKey;
    this.bucket = options.gcloud.bucket;
    this.options = options;
    if (this.pathToSshKey && !options.isPrivateRepo) {
      this.GIT_SSH_COMMAND = "GIT_SSH_COMMAND='ssh -i " + this.pathToSshKey + "'";
    } else {
      this.GIT_SSH_COMMAND = '';
    }
    this.name = path.parse(this.repoUrl).name;
    this.gitPath = path.join(this.fsGitDir, this.name);
    this.errors = [];
    this.stdouts = [];
  }

  BuildBot.prototype.exec = function(command, alias, ondone) {
    return exec(command, (function(_this) {
      return function(error, stdout, stderr) {
        var e, s;
        console.log(alias);
        e = {
          alias: alias
        };
        s = {
          alias: alias
        };
        if (error) {
          e.error = error;
        }
        if (stderr) {
          e.stderr = stderr;
        }
        if (error || stderr) {
          _this.errors.push(e);
        }
        if (stdout) {
          s.stdout = stdout;
          _this.stdouts.push(s);
        }
        console.log(_this.error);
        return ondone(error);
      };
    })(this));
  };

  BuildBot.prototype.fetchAndUpdate = function(ondone) {
    var command;
    if (fs.existsSync(this.gitPath + '/.git')) {
      command = ["cd " + this.gitPath, "git reset --hard", this.GIT_SSH_COMMAND + " git fetch --all"];
      command = command.join('; ');
    } else {
      command = this.GIT_SSH_COMMAND + (" git clone " + this.repoUrl + " " + this.gitPath);
    }
    if (this.options.hash) {
      command += "; git checkout " + this.options.hash;
    }
    return this.exec(command, 'fetchAndUpdate', function(error) {
      if (ondone) {
        return ondone(error);
      }
    });
  };

  BuildBot.prototype.cleanLocalRepo = function(ondone) {
    var command, data, has_clean_target, j, len, line, ref;
    has_clean_target = false;
    if (fs.existsSync(this.gitPath + '/Makefile')) {
      data = fs.readFileSync(this.gitPath + '/Makefile', 'utf8');
      ref = data.split('\n');
      for (j = 0, len = ref.length; j < len; j++) {
        line = ref[j];
        if (line.indexOf('clean:') === 0 || line.indexOf('clean :') === 0) {
          has_clean_target = true;
          continue;
        }
      }
    }
    if (!has_clean_target) {
      if (ondone) {
        ondone();
      }
      return;
    }
    command = "cd " + this.gitPath + "; make clean";
    return this.exec(command, 'cleanLocalRepo', function(error) {
      if (ondone) {
        return ondone(error);
      }
    });
  };

  BuildBot.prototype.buildSite = function(ondone) {
    var command, node_modules_dir;
    node_modules_dir = this.gitPath + '/node_modules';
    fs.removeSync(node_modules_dir);
    fs.ensureSymlinkSync(this.nodeModulesDir, node_modules_dir);
    command = "cd " + this.gitPath + "; bower install; grunt build-prod;";
    return exec(command, 'buildSite', function(error) {
      if (ondone) {
        return ondone(error);
      }
    });
  };

  BuildBot.prototype.collectTranslations = function(ondone) {
    this.gettextJson = gettextHelpers.potToJson(this.gitPath + "/po/**/*.pot");
    if (ondone) {
      return ondone();
    }
  };

  BuildBot.prototype.build = function(ondone) {
    return this.fetchAndUpdate((function(_this) {
      return function(e) {
        if (e) {
          if (ondone) {
            ondone(e);
          }
          return;
        }
        return _this.cleanLocalRepo(function(e) {
          if (e) {
            if (ondone) {
              ondone(e);
            }
            return;
          }
          return _this.buildSite(function(e) {
            if (e) {
              if (ondone) {
                ondone(e);
              }
              return;
            }
            return _this.collectTranslations(function(e) {
              if (ondone) {
                return ondone(e);
              }
            });
          });
        });
      };
    })(this));
  };

  BuildBot.prototype.deploy = function(ondone) {
    var files, j, len, list, p, patterns, storage;
    patterns = [this.gitPath + '/client/**/*.min.js', this.gitPath + '/client/**/*.min.css', this.gitPath + '/client/**/*.{jpg,png,gif,svg}'];
    files = [];
    for (j = 0, len = patterns.length; j < len; j++) {
      p = patterns[j];
      list = glob.sync(p);
      files = files.concat(list);
    }
    storage = new GCloudStorage(this.options.gcloud);
    return storage.uploadFiles(files, this.gitPath, (function(_this) {
      return function() {
        var bucket, command, gce_zone, k, v, vm;
        if (storage.errors.length) {
          _this.errors.push({
            alias: 'deploy to google cloud',
            error: storage.errors
          });
        }
        gce_zone = _this.options.gcloud.gceZone || 'europe-west1-d';
        vm = _this.options.gcloud.vm || 'shared-hosting--git';
        bucket = _this.options.gcloud.bucket.replace('lbcms-container-', '');
        files = {
          'views/*.html': 'public_html/views/',
          'views/index.html': 'public_html/index.html',
          '_htaccess': 'public_html/.htaccess'
        };
        command = "";
        for (k in files) {
          v = files[k];
          command += "gcloud compute --project " + _this.options.gcloud.projectId + " copy-files " + _this.gitPath + "/" + k + " node-user@" + vm + ":/var/www/" + bucket + "/" + v + " --zone " + gce_zone + "; ";
        }
        return _this.exec(command, "upload files to compute engine", function() {
          if (ondone) {
            return ondone();
          }
        });
      };
    })(this));
  };

  return BuildBot;

})();

module.exports = {
  BuildBot: BuildBot,
  GCloudStorage: GCloudStorage,
  AwsS3Storage: AwsS3Storage
};
