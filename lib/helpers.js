(function() {
  var getValue, getValueOr, isDefined, isEmptyObj, isString, l10n, string;

  isDefined = function(value) {
    return typeof value !== 'undefined';
  };

  isString = function(value) {
    return typeof value === 'string';
  };

  getValue = function(obj, _path, _default) {
    var _p, i, len, ref, res;
    if (obj === null || typeof obj === 'undefined') {
      return _default;
    }
    _path = _path.replace(/\[(\w+)\]/g, '.$1');
    _path = _path.replace(/^\./, '');
    res = obj;
    ref = _path.split('.');
    for (i = 0, len = ref.length; i < len; i++) {
      _p = ref[i];
      if (res === null) {
        return _default || res;
      }
      if (typeof res[_p] !== 'undefined') {
        res = res[_p];
      } else {
        return _default;
      }
    }
    if (typeof res === 'undefined') {
      return _default;
    }
    return res;
  };

  getValueOr = function(obj) {
    var _default, _p, _paths, i, len, v;
    if (arguments.length < 4) {
      throw "Expected 4 arguments min for this function. Got " + arguments.length;
    }
    _paths = Array.prototype.slice.call(arguments, 1, arguments.length - 1);
    _default = arguments[arguments.length - 1];
    for (i = 0, len = _paths.length; i < len; i++) {
      _p = _paths[i];
      v = getValue(obj, _p);
      if (typeof v !== 'undefined') {
        return v;
      }
    }
    return _default;
  };

  isEmptyObj = function(obj) {
    if (typeof obj === 'undefined' || obj === null) {
      return true;
    }
    if (obj instanceof Array || typeof obj === 'string') {
      return obj.length === 0;
    }
    if (typeof obj !== 'object') {
      return false;
    }
    return Object.keys(obj).length === 0;
  };

  l10n = function(l10nObject, forceLang, defaultLang) {
    var k, lang, ref, text, v;
    if (!l10nObject) {
      return '';
    }
    if (forceLang) {
      lang = forceLang;
    } else {
      lang = defaultLang;
    }
    if (l10nObject.__data) {
      l10nObject = l10nObject.__data;
    }
    text = getValueOr(l10nObject, lang, "__data." + lang, null);
    if (text) {
      return text;
    }
    defaultLang = defaultLang || 'en';
    text = getValueOr(l10nObject, defaultLang, "__data." + defaultLang, null);
    if (text) {
      return text;
    }
    ref = l10nObject || {};
    for (k in ref) {
      v = ref[k];
      if (isString(v) && v.length) {
        return v;
      }
    }
    return "";
  };

  string = {
    matchAll: function(target, regexp) {
      var matches;
      matches = [];
      target.replace(regexp, function() {
        var arr, extras;
        arr = [].slice.call(arguments, 0);
        extras = arr.splice(-2);
        arr.index = extras[0];
        arr.input = extras[1];
        matches.push(arr);
      });
      if (matches.length) {
        return matches;
      } else {
        return null;
      }
    },
    replaceAll: function(target, search, replacement) {
      if (search instanceof RegExp) {
        target = target.replace(new RegExp(search, 'g'), replacement);
      } else {
        search = search + '';
        target = target.split(search).join(replacement);
      }
      return target;
    }
  };

  module.exports = {
    getValue: getValue,
    getValueOr: getValueOr,
    l10n: l10n,
    string: string,
    isEmptyObj: isEmptyObj,
    isDefined: isDefined
  };

}).call(this);
