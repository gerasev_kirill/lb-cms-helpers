
request = require('request')

class Q
    constructor: (@name)->
        self = @
        @done = false
        @promise = {
            then: (onSuccess, onErr)->
                if self.rejected and !self.done
                    self.done = true
                    return onErr(self.data)
                if self.resolved and !self.done
                    self.done = true
                    return onSuccess(self.data)
                self.onErr = onErr
                self.onSuccess = onSuccess
                return
        }

    resolve: (data)->
        @resolved = true
        @data = data
        if @onSuccess and !@done
            @done = true
            @onSuccess(data)
        return
    reject: (data)->
        @rejected = true
        @data = data
        if @onErr and !@done
            @done = true
            @onErr(data)
        return



$http = {
    get: (url)->
        defer = new Q('http')
        options = {
            url
            headers:{
                "Accept": "application/json"
                "Content-Type": "application/json"
                "Referer": "http://localhost:3000"
            }
        }
        request.get options, (err, res, body)->
            if err
                return defer.reject(err)
            data = body
            try
                data = JSON.parse(body)
            catch e
            defer.resolve(data)
        defer.promise
}



String::matchAll = (regexp) ->
    matches = []
    @replace regexp, ->
        arr = [].slice.call(arguments, 0)
        extras = arr.splice(-2)
        arr.index = extras[0]
        arr.input = extras[1]
        matches.push(arr)
        return
    if matches.length then matches else null



String::replaceAll = (search, replacement) ->
    target = this
    if search instanceof RegExp
        target = target.replace(new RegExp(search, 'g'), replacement)
    else
        search = angular.copy(search) + ''
        target = target.split(search).join(replacement)
    target







module.exports = {
    $http: $http
    $q: {
        defer: ()->
            new Q()
    }
}
