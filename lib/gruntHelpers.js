(function() {
  var _, cheerio, css, fs, generateBowerLibsList, generateFilePattern, glob, path, processFilesForCloudStorage,
    indexOf = [].indexOf;

  fs = require('fs');

  path = require('path');

  cheerio = require('cheerio');

  glob = require('glob');

  css = require('css');

  _ = require('underscore');

  generateFilePattern = function(dirs, ext) {
    var d, i, len, list;
    if (!ext) {
      ext = '.js';
    }
    list = [];
    for (i = 0, len = dirs.length; i < len; i++) {
      d = dirs[i];
      if (indexOf.call(d, '*') >= 0) {
        list.push('client/' + d);
        continue;
      }
      if (d.indexOf('bower_components') === 0) {
        if (path.extname(d) === ext) {
          list.push(d);
        }
        continue;
      }
      list.push('client/' + d + `/**/module*${ext}`);
      list.push('client/' + d + `/**/!(module*|*-spec)${ext}`);
    }
    return list;
  };

  generateBowerLibsList = function(src, ext, bowerJson) {
    /*
        возвращает список путей к файлам по зависимостям bower-a.
        благодаря передаче bowerJson как параметра, можно держать
        несколько index.pug с разными bower.*.json файлами зависимостей.
        @param {string} src путь к index.pug файлу для которого генерируется список
        @param {string=} [ext='js'] тип файлов ['js', 'css']
        @param {object=} bowerJson содержимое bower.*.json файла
        @returns {Array<string>} список путей к файлам по зависимостям bower-a
    */
    var props, wiredep;
    props = {
      src,
      ignorePath: '..'
    };
    if (ext !== 'css' && ext !== 'js') {
      ext = 'js';
    }
    if (bowerJson) {
      props.bowerJson = bowerJson;
    }
    wiredep = require('wiredep')(props);
    if (!wiredep[ext]) {
      return [];
    }
    return wiredep[ext];
  };

  processFilesForCloudStorage = function(html_list, white_list, bucket, cloudStorageUrl, prefix) {
    /*
        заменяет все ссылки в html файлах к js, image, video файлам лежащим
        локально на пути к Google Cloud Storage

        html_list = ['views/index.html', 'views/admin.html']
        white_list = ['client/*.png', 'client/*.jpg']
        bucket = 'my_bucket'
    */
    var $, _list, _wl, cleanCssUrl, cleanLinks, css_content, css_links, css_urls, declaration, f, furl, html_content, i, images, j, k, len, len1, len2, len3, len4, len5, links, list, m, n, o, obj, readAll, ref, ref1, ref2, ref3, replaceAll, rule, s, scripts, videos, w;
    _wl = [];
    for (i = 0, len = white_list.length; i < len; i++) {
      w = white_list[i];
      list = glob.sync(w);
      _wl = _wl.concat(list);
    }
    white_list = [];
    for (j = 0, len1 = _wl.length; j < len1; j++) {
      w = _wl[j];
      if (w[0] !== '/' && w[0] !== '.') {
        w = '/' + w;
      }
      white_list.push(w);
    }
    readAll = function(file_list) {
      var content, f, k, len2, len3, m, p;
      // считываем все файлы из списка. для cheerio и css
      content = '';
      for (k = 0, len2 = file_list.length; k < len2; k++) {
        p = file_list[k];
        if (p[0] === '/') {
          p = '.' + p;
        }
        list = glob.sync(p);
        for (m = 0, len3 = list.length; m < len3; m++) {
          f = list[m];
          content += fs.readFileSync(f);
        }
      }
      return content;
    };
    replaceAll = function(file_list, replace_list, options) {
      var data, ext, k, l, len2, newUrl, p, r, results;
      if (!options) {
        options = {
          prepend: ''
        };
      }
      if (!options.prepend) {
        options.prepend = {
          r: '',
          text: ''
        };
      }
      results = [];
      for (k = 0, len2 = file_list.length; k < len2; k++) {
        p = file_list[k];
        if (p[0] === '/') {
          p = '.' + p;
        }
        list = glob.sync(p);
        results.push((function() {
          var len3, len4, m, n, ref, results1;
          results1 = [];
          for (m = 0, len3 = list.length; m < len3; m++) {
            l = list[m];
            ext = path.extname(l);
            data = fs.readFileSync(l);
            data = data.toString();
            if (ext === '.css') {
              data = data.split('./client/');
              data = data.join('/client/');
              data = data.split('/client/');
              data = data.join(`${cloudStorageUrl}${bucket}/client/`);
            } else {
              ref = _.union(replace_list, white_list);
              for (n = 0, len4 = ref.length; n < len4; n++) {
                r = ref[n];
                if (prefix) {
                  newUrl = path.join(prefix, r);
                  newUrl = `${cloudStorageUrl}${bucket}${newUrl}`;
                } else {
                  newUrl = `${cloudStorageUrl}${bucket}${r}`;
                }
                data = data.replace(new RegExp(r, 'g'), newUrl);
              }
            }
            results1.push(fs.writeFileSync(l, data));
          }
          return results1;
        })());
      }
      return results;
    };
    cleanLinks = function(file_list) {
      var _file_list, k, l, len2, ref, ref1;
      // очищаем ссылки на файлы. если в них есть http часть, то они не
      // участвуют в автозамене. также пропускаем те, в которых есть jade переменные
      // или те, которые есть просто data-base64
      _file_list = [];
      for (k = 0, len2 = file_list.length; k < len2; k++) {
        l = file_list[k];
        if (l.indexOf('{%') === 0) {
          // django
          l = l.split('static ')[1].split('%}')[0].trim();
          if ((ref = l[0]) === '"' || ref === "'") {
            l = l.substr(1);
          }
          if ((ref1 = l[l.length - 1]) === '"' || ref1 === "'") {
            l = l.substr(0, l.length - 1);
          }
          console.log(l);
        }
        if (l.indexOf('http://') > -1 || l.indexOf('https://') > -1 || _file_list.indexOf(l) > -1 || l.indexOf('data:;base64') > -1 || (l.indexOf('data:application/') > -1 && l.indexOf(';base64,') > -1) || l.indexOf('data:image/') === 0 || l.indexOf('{{') > -1) {
          continue;
        }
        _file_list.push(l);
      }
      return _file_list;
    };
    cleanCssUrl = function(str_list) {
      var __s, _s, _str, cleaned, k, len2, len3, len4, m, n, ref, ref1, ref2;
      // очищаем найденый список строк с url из css
      cleaned = [];
      for (k = 0, len2 = str_list.length; k < len2; k++) {
        _str = str_list[k];
        _str = _str.split(',');
        for (m = 0, len3 = _str.length; m < len3; m++) {
          _s = _str[m];
          ref = _s.split(' ');
          for (n = 0, len4 = ref.length; n < len4; n++) {
            __s = ref[n];
            if (__s.indexOf('url') !== 0) {
              continue;
            }
            if (__s.indexOf('data:image/') > -1) {
              continue;
            }
            if (__s.indexOf('data:application/') > -1) {
              continue;
            }
            __s = __s.replace('url(', '').replace(')', '');
            __s = __s.split('#')[0].split('?')[0];
            if ((ref1 = __s[0]) === '"' || ref1 === "'") {
              __s = __s.substr(1);
            }
            if ((ref2 = __s[__s.length - 1]) === '"' || ref2 === "'") {
              __s = __s.substr(0, __s.length - 1);
            }
            cleaned.push(__s);
          }
        }
      }
      return cleaned;
    };
    html_content = readAll(html_list);
    $ = cheerio.load(html_content);
    // находим весь js
    scripts = (function() {
      var k, len2, ref, results;
      ref = $('script');
      results = [];
      for (k = 0, len2 = ref.length; k < len2; k++) {
        s = ref[k];
        if (s.attribs.src) {
          results.push(s.attribs.src.split('?')[0]);
        }
      }
      return results;
    })();
    scripts = cleanLinks(scripts);
    // находим все img
    images = (function() {
      var k, len2, ref, results;
      ref = $('img');
      results = [];
      for (k = 0, len2 = ref.length; k < len2; k++) {
        s = ref[k];
        if (s.attribs.src) {
          results.push(s.attribs.src.split('?')[0]);
        }
      }
      return results;
    })();
    images = cleanLinks(images);
    // находим все ссылки на видео
    videos = [];
    ref = $('video').find('source');
    for (k = 0, len2 = ref.length; k < len2; k++) {
      s = ref[k];
      if (s.attribs.src) {
        videos.push(s.attribs.src.split('?')[0]);
      }
    }
    videos = cleanLinks(videos);
    // находим все ссылки на links
    links = (function() {
      var len3, m, ref1, results;
      ref1 = $('link');
      results = [];
      for (m = 0, len3 = ref1.length; m < len3; m++) {
        s = ref1[m];
        if (s.attribs.href) {
          results.push(s.attribs.href.split('?')[0]);
        }
      }
      return results;
    })();
    links = cleanLinks(links);
    // ищем ссылки на внешние источники в css
    css_links = links.filter(function(l) {
      return path.extname(l) === '.css';
    });
    css_content = readAll(css_links);
    obj = css.parse(css_content);
    css_urls = [];
    ref1 = obj.stylesheet.rules;
    for (m = 0, len3 = ref1.length; m < len3; m++) {
      rule = ref1[m];
      if (!rule.declarations) {
        continue;
      }
      ref2 = rule.declarations;
      for (n = 0, len4 = ref2.length; n < len4; n++) {
        declaration = ref2[n];
        if (declaration.value && declaration.value.indexOf('url(') > -1) {
          furl = declaration.value.split('url(')[1].split(')')[0];
          if (furl === 'none' || furl.indexOf('data:image') > -1) {
            continue;
          }
          css_urls.push(declaration.value);
        }
      }
    }
    // очищаем ссылки из css
    css_urls = cleanCssUrl(css_urls);
    css_urls = cleanLinks(css_urls);
    // заменяем все в файлах
    replaceAll(html_list, _.union(scripts, links, images, videos));
    replaceAll(links.filter(function(l) {
      var ref3;
      return (ref3 = path.extname(l)) === '.json' || ref3 === '.css';
    }), css_urls);
    _list = [];
    ref3 = _.union(white_list, scripts, links, css_urls);
    for (o = 0, len5 = ref3.length; o < len5; o++) {
      f = ref3[o];
      if (f[0] === '/') {
        _list.push(f.substring(1));
        continue;
      }
      _list.push(f);
    }
    return _list;
  };

  module.exports = {
    generateFilePattern,
    generateBowerLibsList,
    processFilesForCloudStorage,
    customJSGettextFinders: [
      {
        isGettext: function(node) {
          if (!(node && node.type === 'ObjectProperty' && node.key.name === '$pageTitle')) {
            return false;
          }
          if (node.value.type === 'ArrayExpression' && node.value.elements.length === 2 && node.value.elements[1].type === 'StringLiteral') {
            return true;
          }
          return node.value.type === 'StringLiteral';
        },
        getJSData: function(node) {
          if (node.value.type === 'StringLiteral') {
            return {
              singular: node.value.value
            };
          }
          return {
            singular: node.value.elements[1].value
          };
        }
      },
      {
        isGettext: function(node) {
          // headerName in $tableViewProvider.config
          return node && node.type === 'ObjectProperty' && node.key.name === 'headerName' && node.value.type === 'StringLiteral';
        },
        getJSData: function(node) {
          return {
            singular: node.value.value
          };
        }
      },
      {
        isGettext: function(node) {
          // angular.tr('hi!')
          if (node && node.type === 'CallExpression' && node.callee && node.callee.type === 'MemberExpression' && node.callee.object) {
            if (node.callee.object.name === 'angular' && node.callee.property && node.callee.property.name === 'tr') {
              if (node.arguments && node.arguments.length === 1) {
                return node.arguments[0].type === 'StringLiteral';
              }
            }
          }
          return false;
        },
        getJSData: function(node) {
          return {
            singular: node.arguments[0].value
          };
        }
      },
      {
        isGettext: function(node) {
          // $cms.showNotification('hi!')
          if (node && node.type === 'CallExpression' && node.callee && node.callee.type === 'MemberExpression' && node.callee.object) {
            if (node.callee.object.name === '$cms' && node.callee.property && node.callee.property.name === 'showNotification') {
              if (node.arguments && node.arguments.length >= 1) {
                return node.arguments[0].type === 'StringLiteral';
              }
            }
          }
          return false;
        },
        getJSData: function(node) {
          return {
            singular: node.arguments[0].value
          };
        }
      }
    ]
  };

}).call(this);
