fs = require('fs')
path = require('path')
cheerio = require('cheerio')
glob = require('glob')
css = require('css')
_ = require('underscore')



generateFilePattern = (dirs, ext)->
    ###
        возвращает нормализированный список js/less файлов для директорий.
        нам нужно, чтоб файл module.js из директорий грузился первым,
        т.к. там идет объявление angular.module('name', []),
        но и для less файлов его можно использовать

        @param {Array<string>} dirs список названий папок из ./client
        @param {string=} [ext='.js']
        @returns {Array<string>} список blob паттернов
    ###
    if !ext then ext = '.js'
    list=[]
    for d in dirs
        if '*' in d
            list.push('client/'+d)
            continue
        if d.indexOf('bower_components') == 0
            if path.extname(d) == ext
                list.push(d)
            continue
        list.push('client/'+d+"/**/module*#{ext}")
        list.push('client/'+d+"/**/!(module*|*-spec)#{ext}")
    list


generateBowerLibsList = (src, ext, bowerJson)->
    ###
        возвращает список путей к файлам по зависимостям bower-a.
        благодаря передаче bowerJson как параметра, можно держать
        несколько index.pug с разными bower.*.json файлами зависимостей.
        @param {string} src путь к index.pug файлу для которого генерируется список
        @param {string=} [ext='js'] тип файлов ['js', 'css']
        @param {object=} bowerJson содержимое bower.*.json файла
        @returns {Array<string>} список путей к файлам по зависимостям bower-a
    ###
    props = {
        src,
        ignorePath: '..'
    }
    if ext not in ['css', 'js']
        ext = 'js'
    if bowerJson
        props.bowerJson = bowerJson
    wiredep = require('wiredep')(props)

    if not wiredep[ext]
        return []
    wiredep[ext]



processFilesForCloudStorage = (html_list, white_list, bucket, cloudStorageUrl, prefix)->
    ###
        заменяет все ссылки в html файлах к js, image, video файлам лежащим
        локально на пути к Google Cloud Storage

        html_list = ['views/index.html', 'views/admin.html']
        white_list = ['client/*.png', 'client/*.jpg']
        bucket = 'my_bucket'
    ###
    _wl = []
    for w in white_list
        list = glob.sync(w)
        _wl = _wl.concat(list)

    white_list = []
    for w in _wl
        if w[0] !='/' and w[0] != '.'
            w = '/'+w
        white_list.push(w)


    readAll = (file_list)->
        # считываем все файлы из списка. для cheerio и css
        content = ''
        for p in file_list
            if p[0] == '/' then p = '.'+p
            list = glob.sync(p)
            for f in list
                content += fs.readFileSync(f)
        content


    replaceAll = (file_list, replace_list, options)->
        if !options then options = {prepend:''}
        if !options.prepend
            options.prepend = {r:'',text:''}
        for p in file_list
            if p[0] == '/' then p = '.'+p
            list = glob.sync(p)
            for l in list
                ext = path.extname(l)
                data = fs.readFileSync(l)
                data = data.toString()
                if ext == '.css'
                    data = data.split('./client/')
                    data = data.join('/client/')
                    data = data.split('/client/')
                    data = data.join("#{cloudStorageUrl}#{bucket}/client/")
                else
                    for r in _.union(replace_list, white_list)
                        if prefix
                            newUrl = path.join(prefix, r)
                            newUrl = "#{cloudStorageUrl}#{bucket}#{newUrl}"
                        else
                            newUrl = "#{cloudStorageUrl}#{bucket}#{r}"
                        data = data.replace(
                            new RegExp(r,'g'),
                            newUrl
                        )
                fs.writeFileSync(l, data)


    cleanLinks = (file_list)->
        # очищаем ссылки на файлы. если в них есть http часть, то они не
        # участвуют в автозамене. также пропускаем те, в которых есть jade переменные
        # или те, которые есть просто data-base64
        _file_list = []
        for l in file_list
            if l.indexOf('{%') == 0
                # django
                l = l.split('static ')[1].split('%}')[0].trim()
                if l[0] in ['"', "'"]
                    l = l.substr(1)
                if l[l.length-1]  in ['"', "'"]
                    l = l.substr(0, l.length-1)
                console.log l
            if l.indexOf('http://')>-1 or l.indexOf('https://')>-1  or \
                    _file_list.indexOf(l) > -1  or \
                    l.indexOf('data:;base64')>-1 or \
                    (l.indexOf('data:application/')>-1 and l.indexOf(';base64,')>-1) or \
                    l.indexOf('data:image/') == 0 or \
                    l.indexOf('{{') > -1
                continue
            _file_list.push(l)
        _file_list

    cleanCssUrl = (str_list)->
        # очищаем найденый список строк с url из css
        cleaned = []
        for _str in str_list
            _str = _str.split(',')
            for _s in _str
                for __s in _s.split(' ')
                    if __s.indexOf('url')!=0 then continue
                    if __s.indexOf('data:image/')>-1 then continue
                    if __s.indexOf('data:application/')>-1 then continue
                    __s = __s.replace('url(', '').replace(')','')
                    __s = __s.split('#')[0].split('?')[0]
                    if __s[0] in ['"', "'"]
                        __s = __s.substr(1)
                    if __s[__s.length-1] in ['"', "'"]
                        __s = __s.substr(0, __s.length-1)
                    cleaned.push(__s)
        cleaned


    html_content = readAll(html_list)
    $ = cheerio.load(html_content)
    # находим весь js
    scripts = (s.attribs.src.split('?')[0]   for s in $('script')   when s.attribs.src)
    scripts = cleanLinks(scripts)
    # находим все img
    images = (s.attribs.src.split('?')[0]   for s in $('img')   when s.attribs.src)
    images = cleanLinks(images)
    # находим все ссылки на видео
    videos = []
    for s in $('video').find('source')
        if s.attribs.src
            videos.push(s.attribs.src.split('?')[0])
    videos = cleanLinks(videos)

    # находим все ссылки на links
    links = (s.attribs.href.split('?')[0]   for s in $('link')   when s.attribs.href)
    links = cleanLinks(links)
    # ищем ссылки на внешние источники в css
    css_links = links.filter (l)->
        path.extname(l)=='.css'
    css_content = readAll(css_links)
    obj = css.parse(css_content)
    css_urls = []

    for rule in obj.stylesheet.rules
        if not rule.declarations
            continue
        for declaration in rule.declarations
            if declaration.value and declaration.value.indexOf('url(') > -1
                furl = declaration.value.split('url(')[1].split(')')[0]
                if furl == 'none' or furl.indexOf('data:image') > -1
                    continue
                css_urls.push(declaration.value)

    # очищаем ссылки из css
    css_urls = cleanCssUrl(css_urls)
    css_urls = cleanLinks(css_urls)

    # заменяем все в файлах
    replaceAll(html_list, _.union(scripts, links, images, videos))
    replaceAll(
        links.filter((l)-> path.extname(l) in ['.json', '.css']),
        css_urls
    )


    _list = []
    for f in _.union(white_list, scripts, links, css_urls)
        if f[0] == '/'
            _list.push(f.substring(1))
            continue
        _list.push(f)
    _list






module.exports = {
    generateFilePattern
    generateBowerLibsList
    processFilesForCloudStorage
    customJSGettextFinders: [{
        isGettext: (node) ->
            # $pageTitle
            if !(node and node.type == 'ObjectProperty' and node.key.name == '$pageTitle')
                return false
            if node.value.type == 'ArrayExpression' and node.value.elements.length == 2 and node.value.elements[1].type == 'StringLiteral'
                return true
            node.value.type == 'StringLiteral'
        getJSData: (node) ->
            if node.value.type == 'StringLiteral'
                return { singular: node.value.value }
            { singular: node.value.elements[1].value }

        },{
            isGettext: (node) ->
                # headerName in $tableViewProvider.config
                node and node.type == 'ObjectProperty' and node.key.name == 'headerName' and node.value.type == 'StringLiteral'
            getJSData: (node)->{
                singular: node.value.value
            }

        },{
            isGettext: (node) ->
                # angular.tr('hi!')
                if node and node.type == 'CallExpression' and node.callee and node.callee.type == 'MemberExpression' and node.callee.object
                    if node.callee.object.name == 'angular' and node.callee.property and node.callee.property.name == 'tr'
                        if node.arguments and node.arguments.length == 1
                          return node.arguments[0].type == 'StringLiteral'
                false
            getJSData: (node)->{
                singular: node.arguments[0].value
            }

        },{
            isGettext: (node) ->
                # $cms.showNotification('hi!')
                if node and node.type == 'CallExpression' and node.callee and node.callee.type == 'MemberExpression' and node.callee.object
                    if node.callee.object.name == '$cms' and node.callee.property and node.callee.property.name == 'showNotification'
                        if node.arguments and node.arguments.length >= 1
                            return node.arguments[0].type == 'StringLiteral'
                false

            getJSData: (node)->{
                singular: node.arguments[0].value
            }
    }]
}
