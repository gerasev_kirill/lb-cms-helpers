try
    log = require('logging').from(__filename)
catch error
    log = console.log




module.exports = (grunt)->
    gettextHelpers = require('../gettextHelpers')
    request = require('request')

    grunt.registerMultiTask 'send_gettext_pot_to_server', ()->
        options = @options({})
        done = @async()
        files = []

        @files.forEach (filePair) =>
            filePair.src.forEach (src) =>
                if filePair.cwd
                    srcFile = if @data.expand then src else filePair.cwd + '/' + src
                else
                    srcFile = src
                files.push(srcFile)

        pot = null
        for f in files
            res = gettextHelpers.potToJson(f, {withReferences: true})
            if pot
                pot.msgs = gettextHelpers.mergeTranslationMsgs(pot.msgs, res.msgs)
            else
                pot = res

        # убираем поле о скрытости перевода (оно надо только на серверной стороне)
        pot.msgs = pot.msgs.map (msg)->
            delete msg.hidden
            msg


        server = options.server or 'http://localhost:8000'
        if server[server.length-1] != '/'
            server = server + '/'
        url = server + 'api/v1/CmsTranslations/set/?x_db_bucket=' + options.bucket + '&langs=' + encodeURIComponent(JSON.stringify(options.langs or []))

        console.log pot.msgs[0]
        request.put url, {
            json: pot
        }, (err, response, body)->
            log body, err
            if err or response.statusCode != 200
                log "[Can't upload pot to #{url}]"
                log body
            done()
