(function() {
  module.exports = function(grunt) {
    var gruntHelpers;
    gruntHelpers = require('../gruntHelpers');
    return grunt.registerMultiTask('prepare_for_cdn', 'Replaces src attribs from local files to cdn cloud storage links', function() {
      var cdnUrl, files, options, server;
      options = this.options({
        storage: {},
        bucket: '',
        white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
      });
      cdnUrl = null;
      if (options.storage.type === 'google') {
        cdnUrl = `${options.storage.server}/lbcms-container-`;
      }
      if (options.storage.type === 'digitaloceanSpaces') {
        server = options.storage.server;
        cdnUrl = `${options.storage.server}/lbcms-container-`;
      }
      if (!cdnUrl) {
        return;
      }
      files = [];
      this.files.forEach((filePair) => {
        return filePair.src.forEach((src) => {
          var srcFile;
          srcFile = this.data.expand ? src : filePair.cwd + '/' + src;
          return files.push(srcFile);
        });
      });
      return gruntHelpers.processFilesForCloudStorage(files, options.white_list, options.bucket, cdnUrl);
    });
  };

}).call(this);
