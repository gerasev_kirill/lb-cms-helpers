

module.exports = (grunt) ->
    fs = require('fs')
    gettextHelpers = require('../gettextHelpers')
    gruntHelpers = require('../gruntHelpers')
    buildHelpers = require('../buildHelpers')

    grunt.registerTask '__gcloud', '', ()->
        options = grunt.config.get('__gcloud')
        storage = new buildHelpers.GCloudStorage({
            keyFilename: options.keyFilename
            projectId: options.projectId
            bucket: "lbcms-container-#{options.bucket}"
        })
        done = @async()
        storage.uploadFiles(options.filesToDeploy, '', done)




    grunt.registerMultiTask 'deploy_to_gcloud', 'Replaces src attribs from local files to google cloud storage https links and uploads files', ()->
        options = @options({
            bucket: ''
            white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
            keyFilename: '.gcloud.json'
        })
        files = []
        @files.forEach (filePair) =>
            filePair.src.forEach (src) =>
                srcFile = if @data.expand then src else filePair.cwd + '/' + src
                files.push(srcFile)

        options.filesToDeploy = gruntHelpers.processFilesForCloudStorage(
            files,
            options.white_list,
            options.bucket,
            "https://storage.googleapis.com/lbcms-container-"
        )
        gettextJson = gettextHelpers.potToJson("po/**/*.pot")
        grunt.file.write('./gettext.json', JSON.stringify(gettextJson))

        postTasks = options.postTasks or []
        postTasks.push('__gcloud')
        grunt.config.set('__gcloud', options)
        grunt.task.run(postTasks)
