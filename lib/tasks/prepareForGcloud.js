(function() {
  module.exports = function(grunt) {
    var buildHelpers, fs, gettextHelpers, gruntHelpers, mkdirp, path, rimraf;
    fs = require('fs');
    path = require('path');
    mkdirp = require('mkdirp');
    rimraf = require('rimraf');
    gettextHelpers = require('../gettextHelpers');
    gruntHelpers = require('../gruntHelpers');
    buildHelpers = require('../buildHelpers');
    grunt.registerTask('__copy', '', function() {
      var done, options;
      options = grunt.config.get('__copy');
      done = this.async();
      rimraf(options.dest + '/*', function() {
        var f, i, len, ref;
        ref = options.files;
        for (i = 0, len = ref.length; i < len; i++) {
          f = ref[i];
          mkdirp.sync('./' + path.dirname(path.join(options.dest, f)));
          fs.writeFileSync(path.join(options.dest, f), fs.readFileSync(f));
        }
        return done();
      });
    });
    return grunt.registerMultiTask('prepare_for_gcloud', 'Replaces src attribs from local files to google cloud storage https links', function() {
      var files, filesToDeploy, gettextJson, options, postTasks;
      options = this.options({
        bucket: '',
        dest: '',
        white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
      });
      files = [];
      this.files.forEach((function(_this) {
        return function(filePair) {
          return filePair.src.forEach(function(src) {
            var srcFile;
            srcFile = _this.data.expand ? src : filePair.cwd + '/' + src;
            return files.push(srcFile);
          });
        };
      })(this));
      filesToDeploy = gruntHelpers.processFilesForCloudStorage(files, options.white_list || [], options.bucket, "https://storage.googleapis.com/lbcms-container-");
      gettextJson = gettextHelpers.potToJson("po/**/*.pot");
      grunt.file.write('./gettext.json', JSON.stringify(gettextJson));
      postTasks = options.postTasks || [];
      if (options.dest) {
        postTasks.push('__copy');
        grunt.config.set('__copy', {
          dest: options.dest,
          files: filesToDeploy.concat(files.filter(function(f) {
            return f.indexOf('./views') === 0;
          }))
        });
      }
      return grunt.task.run(postTasks);
    });
  };

}).call(this);
