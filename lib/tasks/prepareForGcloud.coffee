



module.exports = (grunt) ->
    fs = require('fs')
    path = require('path')
    mkdirp = require('mkdirp')
    rimraf = require('rimraf')
    gettextHelpers = require('../gettextHelpers')
    gruntHelpers = require('../gruntHelpers')
    buildHelpers = require('../buildHelpers')

    grunt.registerTask '__copy', '', ()->
        options = grunt.config.get('__copy')
        done = @async()
        rimraf options.dest+'/*', ()->
            for f in options.files
                mkdirp.sync('./'+path.dirname(path.join(options.dest, f)))
                fs.writeFileSync(
                    path.join(options.dest, f),
                    fs.readFileSync(f)
                )
            done()
        return



    grunt.registerMultiTask 'prepare_for_gcloud', 'Replaces src attribs from local files to google cloud storage https links', ()->
        options = @options({
            bucket: ''
            dest: ''
            white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
        })
        files = []
        @files.forEach (filePair) =>
            filePair.src.forEach (src) =>
                srcFile = if @data.expand then src else filePair.cwd + '/' + src
                files.push(srcFile)

        filesToDeploy = gruntHelpers.processFilesForCloudStorage(
            files,
            options.white_list or [],
            options.bucket,
            "https://storage.googleapis.com/lbcms-container-"
        )
        gettextJson = gettextHelpers.potToJson("po/**/*.pot")
        grunt.file.write('./gettext.json', JSON.stringify(gettextJson))

        postTasks = options.postTasks or []
        if options.dest
            postTasks.push('__copy')
            grunt.config.set('__copy', {
                dest: options.dest
                files: filesToDeploy.concat(
                    files.filter (f)->
                        f.indexOf('./views') == 0
                )
            })
        grunt.task.run(postTasks)
