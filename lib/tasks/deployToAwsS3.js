module.exports = function(grunt) {
  var buildHelpers, fs, gettextHelpers, gruntHelpers;
  fs = require('fs');
  gettextHelpers = require('../gettextHelpers');
  gruntHelpers = require('../gruntHelpers');
  buildHelpers = require('../buildHelpers');
  return grunt.registerMultiTask('deploy_to_aws_s3', 'Replaces src attribs from local files to aws s3 cloud storage https links and uploads files', function() {
    var done, files, filesToDeploy, options, storage, storageOptions;
    options = this.options({
      bucket: '',
      white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
    });
    done = this.async();
    files = [];
    this.files.forEach((function(_this) {
      return function(filePair) {
        return filePair.src.forEach(function(src) {
          var srcFile;
          srcFile = _this.data.expand ? src : filePair.cwd + '/' + src;
          return files.push(srcFile);
        });
      };
    })(this));
    filesToDeploy = gruntHelpers.processFilesForCloudStorage(files, options.white_list, options.bucket, options.storageEndpoint + "/lbcms-container-", options.prefix);
    storageOptions = {
      accessKeyId: options.accessKeyId,
      secretAccessKey: options.secretAccessKey,
      bucket: "lbcms-container-" + options.bucket,
      storageEndpoint: options.storageEndpoint
    };
    if (options.prefix) {
      storageOptions.filePrefix = options.prefix;
    }
    storage = new buildHelpers.AwsS3Storage(storageOptions);
    return storage.uploadFiles(filesToDeploy, '', function() {
      var postTasks;
      postTasks = options.postTasks || [];
      grunt.task.run(postTasks);
      return done();
    });
  });
};
