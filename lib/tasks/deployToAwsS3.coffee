

module.exports = (grunt) ->
    fs = require('fs')
    gettextHelpers = require('../gettextHelpers')
    gruntHelpers = require('../gruntHelpers')
    buildHelpers = require('../buildHelpers')



    grunt.registerMultiTask 'deploy_to_aws_s3', 'Replaces src attribs from local files to aws s3 cloud storage https links and uploads files', ()->
        options = @options({
            bucket: ''
            white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
        })
        done = @async()
        files = []
        @files.forEach (filePair) =>
            filePair.src.forEach (src) =>
                srcFile = if @data.expand then src else filePair.cwd + '/' + src
                files.push(srcFile)

        filesToDeploy = gruntHelpers.processFilesForCloudStorage(
            files,
            options.white_list,
            options.bucket,
            "#{options.storageEndpoint}/lbcms-container-",
            options.prefix
        )

        storageOptions = {
            accessKeyId: options.accessKeyId
            secretAccessKey: options.secretAccessKey
            bucket: "lbcms-container-#{options.bucket}"
            storageEndpoint: options.storageEndpoint
        }
        if options.prefix
            storageOptions.filePrefix = options.prefix

        storage = new buildHelpers.AwsS3Storage(storageOptions)
        storage.uploadFiles filesToDeploy, '', ()->
            postTasks = options.postTasks or []
            grunt.task.run(postTasks)
            done()
