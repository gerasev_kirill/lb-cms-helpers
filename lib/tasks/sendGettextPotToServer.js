(function() {
  var error, log;

  try {
    log = require('logging').from(__filename);
  } catch (error1) {
    error = error1;
    log = console.log;
  }

  module.exports = function(grunt) {
    var gettextHelpers, request;
    gettextHelpers = require('../gettextHelpers');
    request = require('request');
    return grunt.registerMultiTask('send_gettext_pot_to_server', function() {
      var done, f, files, i, len, options, pot, res, server, url;
      options = this.options({});
      done = this.async();
      files = [];
      this.files.forEach((function(_this) {
        return function(filePair) {
          return filePair.src.forEach(function(src) {
            var srcFile;
            if (filePair.cwd) {
              srcFile = _this.data.expand ? src : filePair.cwd + '/' + src;
            } else {
              srcFile = src;
            }
            return files.push(srcFile);
          });
        };
      })(this));
      pot = null;
      for (i = 0, len = files.length; i < len; i++) {
        f = files[i];
        res = gettextHelpers.potToJson(f, {
          withReferences: true
        });
        if (pot) {
          pot.msgs = gettextHelpers.mergeTranslationMsgs(pot.msgs, res.msgs);
        } else {
          pot = res;
        }
      }
      pot.msgs = pot.msgs.map(function(msg) {
        delete msg.hidden;
        return msg;
      });
      server = options.server || 'http://localhost:8000';
      if (server[server.length - 1] !== '/') {
        server = server + '/';
      }
      url = server + 'api/v1/CmsTranslations/set/?x_db_bucket=' + options.bucket + '&langs=' + encodeURIComponent(JSON.stringify(options.langs || []));
      console.log(pot.msgs[0]);
      return request.put(url, {
        json: pot
      }, function(err, response, body) {
        log(body, err);
        if (err || response.statusCode !== 200) {
          log("[Can't upload pot to " + url + "]");
          log(body);
        }
        return done();
      });
    });
  };

}).call(this);
