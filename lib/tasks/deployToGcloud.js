(function() {

  module.exports = function(grunt) {
    var buildHelpers, fs, gettextHelpers, gruntHelpers;
    fs = require('fs');
    gettextHelpers = require('../gettextHelpers');
    gruntHelpers = require('../gruntHelpers');
    buildHelpers = require('../buildHelpers');
    grunt.registerTask('__gcloud', '', function() {
      var done, options, storage;
      options = grunt.config.get('__gcloud');
      storage = new buildHelpers.GCloudStorage({
        keyFilename: options.keyFilename,
        projectId: options.projectId,
        bucket: "lbcms-container-" + options.bucket
      });
      done = this.async();
      return storage.uploadFiles(options.filesToDeploy, '', done);
    });
    return grunt.registerMultiTask('deploy_to_gcloud', 'Replaces src attribs from local files to google cloud storage https links and uploads files', function() {
      var files, gettextJson, options, postTasks,
        _this = this;
      options = this.options({
        bucket: '',
        white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif'],
        keyFilename: '.gcloud.json'
      });
      files = [];
      this.files.forEach(function(filePair) {
        return filePair.src.forEach(function(src) {
          var srcFile;
          srcFile = _this.data.expand ? src : filePair.cwd + '/' + src;
          return files.push(srcFile);
        });
      });
      options.filesToDeploy = gruntHelpers.processFilesForCloudStorage(files, options.white_list, options.bucket, "https://storage.googleapis.com/lbcms-container-");
      gettextJson = gettextHelpers.potToJson("po/**/*.pot");
      grunt.file.write('./gettext.json', JSON.stringify(gettextJson));
      postTasks = options.postTasks || [];
      postTasks.push('__gcloud');
      grunt.config.set('__gcloud', options);
      return grunt.task.run(postTasks);
    });
  };

}).call(this);
