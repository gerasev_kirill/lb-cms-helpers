

module.exports = (grunt) ->
    gruntHelpers = require('../gruntHelpers')

    grunt.registerMultiTask 'prepare_for_cdn', 'Replaces src attribs from local files to cdn cloud storage links', ()->
        options = @options({
            storage: {}
            bucket: ''
            white_list: ['client/**/*.png', 'client/**/*.jpg', 'client/**/*.gif']
        })
        cdnUrl = null
        if options.storage.type == 'google'
            cdnUrl = "#{options.storage.server}/lbcms-container-"
        if options.storage.type == 'digitaloceanSpaces'
            server = options.storage.server
            cdnUrl = "#{options.storage.server}/lbcms-container-"
        if !cdnUrl
            return

        files = []
        @files.forEach (filePair) =>
            filePair.src.forEach (src) =>
                srcFile = if @data.expand then src else filePair.cwd + '/' + src
                files.push(srcFile)


        gruntHelpers.processFilesForCloudStorage(
            files,
            options.white_list,
            options.bucket,
            cdnUrl
        )
