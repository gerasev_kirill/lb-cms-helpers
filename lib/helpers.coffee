
isDefined = (value)->
    typeof(value)!='undefined'

isString = (value)->
    typeof(value)=='string'


getValue = (obj, _path, _default)->
    if obj == null or typeof(obj) == 'undefined'
        return _default
    _path = _path.replace(/\[(\w+)\]/g, '.$1') # convert indexes to properties
    _path = _path.replace(/^\./, '')           # strip a leading dot
    res = obj
    for _p in _path.split('.')
        if res == null
            return _default or res
        if typeof(res[_p]) != 'undefined'
            res = res[_p]
        else
            return _default
    if typeof(res) == 'undefined'
        return _default
    res



getValueOr = (obj)->
    if arguments.length < 4
        throw "Expected 4 arguments min for this function. Got #{arguments.length}"

    _paths = Array.prototype.slice.call(arguments, 1, arguments.length-1)
    _default = arguments[arguments.length-1]
    for _p in _paths
        v = getValue(obj, _p)
        if typeof(v)!='undefined'
            return v
    _default



isEmptyObj = (obj)->
    if typeof(obj) == 'undefined' or obj == null
        return true
    if obj instanceof Array or typeof(obj) == 'string'
        return obj.length == 0
    if typeof(obj) != 'object'
        return false
    Object.keys(obj).length == 0


l10n = (l10nObject, forceLang, defaultLang)->
    if not l10nObject
        return ''
    if forceLang
        lang = forceLang
    else
        lang = defaultLang
    if l10nObject.__data
        l10nObject = l10nObject.__data
    text = getValueOr(l10nObject, lang, "__data.#{lang}", null)
    if text
        return text
    defaultLang = defaultLang or 'en'
    text = getValueOr(l10nObject, defaultLang, "__data.#{defaultLang}", null)
    if text
        return text
    for k,v of l10nObject or {}
        if isString(v) and v.length
            return v
    ""


string = {
    matchAll: (target, regexp) ->
        matches = []
        target.replace regexp, ->
            arr = [].slice.call(arguments, 0)
            extras = arr.splice(-2)
            arr.index = extras[0]
            arr.input = extras[1]
            matches.push(arr)
            return
        if matches.length then matches else null

    replaceAll: (target, search, replacement) ->
        if search instanceof RegExp
            target = target.replace(new RegExp(search, 'g'), replacement)
        else
            search = search + ''
            target = target.split(search).join(replacement)
        target
}





module.exports = {
    getValue
    getValueOr
    l10n
    string

    isEmptyObj
    isDefined
}
